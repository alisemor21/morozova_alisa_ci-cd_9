import time
import datetime

import mysql.connector



class Exam:
    def __init__(self, lesson, date, teacher, mark):
        self.lesson = lesson
        self.date = date
        self.teacher = teacher
        self.mark = mark


class RecordBook:
    def __init__(self, marks):
        self.marks = marks


class Person:
    def __init__(self, id, firstname, secondname, birth, recordBook):
        self.id = id
        self.firstname = firstname
        self.secondname = secondname
        self.birth = birth
        self.recordBook = recordBook


database = mysql.connector.connect(
    user="root",
    password="mysql",
    host="mysql",
    database="university"
)

cursor = database.cursor()
cursor.execute("use university")

cursor.execute(
    "select person.id, person.firstName, person.secondName, person.dateOfBirth, recordBook.1_lesson, recordBook.1_date, recordBook.1_teacher, recordBook.1_mark, recordBook.2_lesson, recordBook.2_date, recordBook.2_teacher, recordBook.2_mark, recordBook.3_lesson, recordBook.3_date, recordBook.3_teacher, recordBook.3_mark, recordBook.4_lesson, recordBook.4_date, recordBook.4_teacher, recordBook.4_mark, recordBook.5_lesson, recordBook.5_date, recordBook.5_teacher, recordBook.5_mark from person left join recordBook on person.id=recordBook.personID")
students = cursor.fetchall()

students_and_recordBooks = []
dates = []

for student in students:
    student = [i for i in student if i is not None]
    recordBook = student[4:]
    student = student[:4]
    #print(student)

students.sort(key=lambda x: x[3])

for student in students:
    print(student[1], student[2], student[3])




